import random
import numpy as np
import time

intRangeFrom = 1
intRangeTo = 1000
random.seed()

def circular(x):
	if(x > 2):
		x = 3-x
		if x < 0: 
			x *= -1
		circular(x)
	return x

def GenerateMatrix(N,M):
	matrix = [[0]*N for i in range(M)]
	for i in range(N):
		for j in range(M):
			matrix[i][j] = random.randint(intRangeFrom,intRangeTo)
	return matrix

def GenerateVector(N):
	vector = [0]*N
	for i in range(N):
		vector[i] = random.randint(intRangeFrom,intRangeTo)
	return vector

def MatrixMultiplication(first,second):
	N = len(first)
	M = len(second[0])
	result = [[0]*N for i in range(M)]

	for i in range(N):
	   for j in range(M):
		   for k in range(N):
			   result[i][j] += first[i][k] * second[k][j]

	return result

def MatrixToVectorMultiplication(matrix,vector):
	M = len(vector)
	result = [0] * M

	for i in range(M):
	   for j in range(M):
		   result[i] += matrix[i][j] * vector[j]
	return result

def VectorToMatrixMultiplication(vector, matrix):
	M = len(vector)
	result = [[0]*M for i in range(M)]

	for i in range(M):
	   for j in range(M):
		   result[i][j] += vector[i] * matrix[1][j]
	return result

def VectorToVectorMultiplication(vectorA, vectorB):
	M = len(vectorA)
	result = [0] * M

	for i in range(3):
		result[circular(i)] = vectorA[circular(i+1)] * vectorB[circular(i+2)] - vectorA[circular(i+2)] * vectorB[circular(i+1)]
	return result

def TimeTest():
	print("\nTime Test\n")

	matrixSize = 500
	print("matrix size: ", matrixSize,"\n")
	testMatrix = GenerateMatrix(matrixSize,matrixSize)
	testVector = GenerateVector(matrixSize)
	testVector2 = GenerateVector(3)

	start_time = time.time()
	MatrixMultiplication(testMatrix,testMatrix)
	print("My Matrix Multiplication takes: %s seconds\n" % (time.time() - start_time))
	start_time = time.time()
	np.dot(testMatrix,testMatrix)
	print("Numpy Matrix Multiplication takes: %s seconds\n" % (time.time() - start_time))

	start_time = time.time()
	MatrixToVectorMultiplication(testMatrix,testVector)
	print("My Matrix to Vector Multiplication takes: %s seconds\n" % (time.time() - start_time))
	start_time = time.time()
	np.dot(testMatrix,testVector)
	print("Numpy Matrix to Vector Multiplication takes: %s seconds\n" % (time.time() - start_time))

	start_time = time.time()
	VectorToVectorMultiplication(testVector2,testVector2)
	print("My Vector to Vector Multiplication takes: %s seconds\n" % (time.time() - start_time))
	start_time = time.time()
	np.cross(testVector2,testVector2)
	print("Numpy Vector to Vector Multiplication takes: %s seconds\n" % (time.time() - start_time))


#TimeTest()