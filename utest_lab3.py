import unittest
import lab3

class CalcTest(unittest.TestCase):

    def setUp(self):
        self.matrix = [[1,2,3],[4,5,6],[7,8,9]]
        self.vectorA = [4,5,6]
        self.vectorB = [1,2,3]

    def test_m_to_m_mul(self):
        self.assertEqual(lab3.MatrixMultiplication(self.matrix,self.matrix),
         [[30, 36, 42], [66, 81, 96], [102, 126, 150]])
        
    def test_m_to_v_mul(self):
        self.assertEqual(lab3.MatrixToVectorMultiplication(self.matrix, self.vectorA), 
        [32, 77, 122])
        
    def test_v_to_m_mul(self):
        self.assertEqual(lab3.VectorToVectorMultiplication(self.vectorA,self.vectorB), 
        [3, -6, 3])

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(CalcTest)
    unittest.TextTestRunner(verbosity=2).run(suite)

